module.exports = async (mysqlDb, userId, user) => {
  console.log(user, userId);

  let u;
  if (!user) {
    const userTable = await mysqlDb.getTable("users");
    u = await userTable
      .select()
      .where("userID = :userID")
      .bind("userID", userId)
      .execute();

    u = u.fetchOne();
  } else {
    u = user;
  }

  console.log(u);

  const bankrollTable = await mysqlDb.getTable("bankrolls");
  let bankroll = await bankrollTable
    .select()
    .where("userID = :userID")
    .bind("userID", userId)
    .execute();

  bankroll = bankroll.fetchOne();

  console.log(bankroll);
  const settingColl = mysqlDb.getCollection("user_settings");
  let userSettings = await settingColl
    .find("userID = :userID")
    .bind("userID", userId)
    .execute();
  userSettings = userSettings.fetchOne();
  console.log(userSettings);
  return {
    userID: u[0],
    name: u[1],
    surname: u[2],
    email: u[3],
    accessToken: u[5],
    tracker: u[6],
    unitSize: u[7],
    lastUpdated: u[8],
    defaultCurrency: u[9],
    bankRoll: u[10],
    level: u[11],
    tag: u[12],
    discordId: u[13],
    googleId: u[14],
    facebookId: u[15],
    password: null,
    stakingPlan: bankroll
      ? {
          bankRollID: bankroll[0],
          userID: bankroll[1],
          incrementType: [2],
          increment: [3],
          startDate: [4],
          startBank: [5],
          rachet: [6],
        }
      : {},
    settings: { ...userSettings },
  };
};
