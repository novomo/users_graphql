/*
    User resolvers
*/
// resolver functions

const { withFilter } = require("graphql-subscriptions");
const getUser = require("./resolvers/get_user");
const addToken = require("./resolvers/add_token");
const updateUser = require("./resolvers/update_user");
const generateKey = require("./resolvers/generate_key");
const login = require("./resolvers/login");
const logout = require("./resolvers/logout");
const resetPass = require("./resolvers/reset_pass");
const forgottenPass = require("./resolvers/forgotten_pass");
const updateSetting = require("./resolvers/update_settings");
const { pubsub } = require("../../constants/pubsub");

module.exports = {
  Subscription: {
    userChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(["USER_CHANGED"]),
        (payload, variables) => {
          // Only push an update if the comment is on
          // the correct repository for this operation
          //console.log(typeof payload.userChanged._id.toString());
          //console.log(typeof variables.user);
          //console.log(payload.userChanged._id.toString() === variables.user);
          return payload.userChanged._id.toString() === variables.user;
        }
      ),
    },
  },
  Mutation: {
    updateSetting,
    generateKey,
    updateUser,
    login,
    logout,
    forgottenPass,
    resetPass,
    addToken,
  },
  Query: {
    getUser,
  },
};
