/*
    Update User Resolver
*/
// node modules
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const updateCache = require("../../../data/cacher/update_cache");
const compileUser = require("../constants/compile_user");
const { pubsub } = require("../../../constants/pubsub");
const queryBuilder = require("../../../data/mysql/query_builder");
// user defined resolver
const { toTimestamp } = require("../../../node_normalization/numbers");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (_, { inputUser }, { currentUser }) => {
  try {
    //console.log(inputUser);

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const userTable = await mysqlDb.getTable("users");
    let user = await userTable
      .select()
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute();

    user = user.fetchOne();

    if (!user) {
      throw new Error("Not authenicated!");
    }
    if (Object.keys(inputUser).length === 0) {
      user = await compileUser(mysqlDb, currentUser, user ? user : null);
      return {
        ...user,
        password: "",
      };
    }

    // if new password hash before saving.
    if (inputUser.password) {
      inputUser.password = encrypt(args.password.passwrd);
    }

    inputUser.lastUpdate = toTimestamp(new Date());

    let newUser = await queryBuilder(
      userTable,
      "update",
      inputUser,
      null,
      "userID",
      currentUser
    );

    newUser = await compileUser(mysqlDb, currentUser, null);
    //console.log(newUser);
    await updateCache("users", currentUser, {
      ...newUser,
      password: "",
    });
    //console.log({ ...args.inputUser, lastUpdated: toTimestamp(new Date()) });
    //console.log(newUser._doc);

    pubsub.publish("USER_CHANGED", {
      userChanged: { ...newUser, password: "" },
    });
    await session.close();
    if (!newUser) {
      throw new Error("Error saving user update");
    }
    //send new user info after removing password.
    return {
      ...newUser,
      password: "",
    };
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating user",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
