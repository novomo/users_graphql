const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");

const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (_, { ep, email }, __) => {
  try {
    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const userTable = await mysqlDb.getTable("users");
    await userTable
      .update()
      .set("password", ep)
      .where("email = :email")
      .bind("email", email.toLowerCase())
      .execute();
    await session.close();
    return true;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Restting Password",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
