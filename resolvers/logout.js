/*
    Logout Resolver
*/
// User defined modules
const { sendRefreshToken } = require("../../../server_middleware/tokens");
const deleteCache = require("../../../data/cacher/cache_delete");
module.exports = async (_, __, { res, currentUser }) => {
  await deleteCache("users", currentUser);
  await deleteCache("accounts", currentUser);
  try {
    sendRefreshToken(res, "");

    return true;
  } catch (err) {}
};
