/*
    Login Resolver
*/
// node modules

// User defined modules
const {
  createAccessToken,
  createRefreshToken,
  sendRefreshToken,
} = require("../../../server_middleware/tokens");
const decrypt = require("../../../encryption/decrypt");
const updateCache = require("../../../data/cacher/update_cache");
// data modules
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const queryBuilder = require("../../../data/mysql/query_builder");
const initialLevelling = require("../../../gamification/levelling/initial_setup");
const compileUser = require("../constants/compile_user");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (_, args, { res }) => {
  // connect to sql server

  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  let userID;
  const { email, password, googleId, name, surname, facebookId } = args;
  try {
    ////console.log({ email, passwrd })
    let e = email.toLowerCase();

    const userTable = await mysqlDb.getTable("users");
    console.log(userTable);
    let user = await userTable
      .select()
      .where("email = :email")
      .bind("email", e)
      .execute();

    user = await user.fetchOne();
    console.log(user);
    console.log(user[0]);
    if (!user) {
      console.log(mysqlClient);
      userID = await queryBuilder(
        session,
        "insert",
        { ...args, level: 1 },
        "users"
      );

      console.log(userID);
      const settingColl = await mysqlDb.getCollection("user_settings");
      const level_data = initialLevelling();
      await settingColl
        .add({
          notificationSettings: {
            tokens: [],
            betting: { sports: [], bobsValue: 0.0 },
            trading: [],
            task: {
              threshold: 0.0,
              pushThreshold: 0.0,
            },
          },
          offers: { offerTypes: [] },
          levelData: level_data,
          userID: userID,
        })
        .execute();
    } else if (password) {
      //console.log(user.passwrd);
      const { decrypted: saveDecodedPass, app_decrypted: saved_app_decrypted } =
        decrypt(user[4]);
      const { decrypted, app_decrypted } = decrypt(password);
      //console.log(saveDecodedPass, saved_app_decrypted);
      //console.log(decrypted, app_decrypted);
      if (decrypted != saveDecodedPass && app_decrypted != saveDecodedPass) {
        throw new Error("credentials do not match with my database");
      }
      userID = user[0];
    }
    ////console.log(user.password)
    user = await compileUser(mysqlDb, userID, user ? user : null);
    sendRefreshToken(res, createRefreshToken(user));
    await session.close();
    console.log(user);
    await updateCache("users", userID, {
      ...user,
      password: "",
    });
    return {
      accessToken: createAccessToken(user),
      user: {
        ...user,
      },
    };
  } catch (err) {
    console.log(err);
    return {
      accessToken: "",
    };
  }
};
