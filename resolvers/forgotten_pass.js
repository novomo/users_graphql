const nodemailer = require("nodemailer");
const { MY_EMAIL, MY_PASSWORD } = require("../../../env");

const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");

const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (_, { ep, email, OTP }, __) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const userTable = mysqlDb.getTable("users");
  let user = await userTable
    .select(["email"])
    .where("email = :email")
    .bind("email", email)
    .execute();

  user = user.fetchOne();
  await session.close();
  //console.log(MY_EMAIL, MY_PASSWORD);
  try {
    if (!user) {
      throw new Error("Emai not found!");
    }
    return new Promise((resolve, reject) => {
      var transporter = nodemailer.createTransport({
        host: "smtp.ionos.co.uk",
        port: 587,
        secureConnection: false,
        auth: {
          user: MY_EMAIL,
          pass: MY_PASSWORD,
        },
        tls: {
          // do not fail on invalid certs
          rejectUnauthorized: false,
          ciphers: "SSLv3",
        },
      });

      const mail_configs = {
        from: MY_EMAIL,
        to: email,
        subject: "IN4FREEDOM PASSWORD RECOVERY",
        html: `<!DOCTYPE html>
    <html lang="en" >
    <head>
      <meta charset="UTF-8">
      <title>In4Freedom - Password Reset</title>
      
    </head>
    <body>
    <!-- partial:index.partial.html -->
    <div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
      <div style="margin:50px auto;width:70%;padding:20px 0">
        <div style="border-bottom:1px solid #eee">
          <a href="https://www.in4freedom.com" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">InFreedom</a>
        </div>
        <p style="font-size:1.1em">Hi,</p>
        <p>Thank you for using In4Freedom. Use the following OTP to complete your Password Recovery Procedure. OTP is valid for 5 minutes</p>
        <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${OTP}</h2>
        <p style="font-size:0.9em;">Regards,<br />Koding 101</p>
        <hr style="border:none;border-top:1px solid #eee" />
        <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
          <p>In4Freedom Inc</p>
         
        </div>
      </div>
    </div>
    <!-- partial -->
      
    </body>
    </html>`,
      };
      transporter.sendMail(mail_configs, function (error, info) {
        if (error) {
          //console.log(error);
          throw new Error(error);
        }
        //console.log(info);
        return resolve(true);
      });
    });
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Forgot Password Email",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
