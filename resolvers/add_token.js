const { MYSQL_DATABASE } = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");

const { toTimestamp } = require("../../../node_normalization/numbers");

module.exports = async (_, { token }, { currentUser }) => {
  try {
    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const userTable = await mysqlDb.getTable("users");
    const user = await userTable
      .select(["userID"])
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute()
      .fetchOne();

    if (!user) {
      throw new Error("Not authenicated!");
    }

    const settingColl = mysqlDb.getCollection("user_settings", true);
    const userSettings = await settingColl
      .find("userID = :userID")
      .bind("userID", currentUser)
      .execute()
      .fetchOne();

    const tokens = userSettings.notificationSettings.tokens;
    //console.log(tokens);
    const oldToken = tokens.filter((pastToken) => {
      if (token === pastToken) {
        return pastToken;
      }
    });
    //console.log(oldToken);
    if (oldToken.length > 0) {
      return true;
    }
    // Find the document

    settingColl.addOrReplaceOne(userSettings["id"], {
      ...userSettings,
      notificationSettings: {
        ...userSettings.notificationSettings,
        tokens: [...userSettings.notificationSetting.tokens, token],
        lastUpdated: toTimestamp(new Date()),
      },
    });
    await session.close();
    return true;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Adding Token",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
