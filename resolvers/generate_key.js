/* 
    Generate API Key function
*/
// node modules
const uuidAPIKey = require("uuid-apikey");

const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");

const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

const { toTimestamp } = require("../../../node_normalization/numbers");

module.exports = async (_, { email }, context) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    const userTable = await mysqlDb.getTable("users");
    const user = await userTable
      .select(["userID"])
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute()
      .fetchOne();

    if (!user) {
      throw new Error("Not authenicated!");
    }
    const apiKey = uuidAPIKey.create().apiKey;

    await userTable
      .set("accessToken", apiKey)
      .set("lastUpdated", toTimestamp(new Date()))
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute();
    await session.close();
    return apiKey;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Generating API Key",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
