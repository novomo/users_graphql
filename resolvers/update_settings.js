/*
    Login Resolver
*/
// node modules

// User defined modules
const updateCache = require("../../../data/cacher/update_cache");
// data modules
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const compileUser = require("../constants/compile_user");
const { pubsub } = require("../../../constants/pubsub");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (_, { inputSetting }, { currentUser }) => {
  // connect to sql server

  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  console.log(inputSetting);

  try {
    const settingColl = await mysqlDb.getCollection("user_settings");
    console.log(settingColl);
    let user_setting = await settingColl
      .find("userID like :userID")
      .bind("userID", currentUser)
      .execute();
    console.log(user_setting);
    user_setting = user_setting.fetchOne();
    console.log(user_setting);
    if (!user_setting) {
      user_setting = await settingColl
        .add({
          ...inputSetting,
          userID: currentUser,
        })
        .execute();
      console.log(user_setting);
    } else {
      user_setting = await settingColl
        .modify("userID like :userID")
        .bind("userID", currentUser)
        .patch({ ...inputSetting })
        .execute();
      console.log(user_setting);
    }
    user_setting = await settingColl
      .find("userID like :userID")
      .bind("userID", currentUser)
      .execute();
    user_setting = user_setting.fetchOne();
    console.log(user_setting);

    let newUser = await compileUser(mysqlDb, currentUser, null);
    await updateCache("users", currentUser, {
      ...newUser,
      password: "",
    });
    //console.log({ ...args.inputUser, lastUpdated: toTimestamp(new Date()) });
    //console.log(newUser._doc);

    pubsub.publish("USER_CHANGED", {
      userChanged: { ...newUser, password: "" },
    });
    await session.close();
    return true;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating Settings",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
