/*
    Get User Reslover
*/
// Models
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const getFromCache = require("../../../data/cacher/get_from_cache");
// user defined modules
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (_, __, { currentUser }) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE); //console.log(currentUser);
  try {
    console.log(currentUser);
    const userTable = await mysqlDb.getTable("users");
    let user = await userTable
      .select()
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute();

    user = user.fetchOne();

    //console.log({ ...args.inputUser, lastUpdated: toTimestamp(new Date()) });
    //console.log(newUser._doc);
    await session.close();
    if (!user) {
      throw new Error("Not authenicated!");
    } else {
      console.log(await getFromCache("users", currentUser));
      return await getFromCache("users", currentUser);
    }
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Getting User",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
